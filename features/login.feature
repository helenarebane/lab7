Feature: Login form

  Scenario: Access the login form with valid credentials

    Given an anonymous user
    When user submits a valid login page
    Then user is redirected to / page

  Scenario: Access the login form with invalid credentials

    Given an anonymous user
    When user submits an invalid login page
    Then user is notified with login failed message