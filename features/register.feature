Feature: Registration form

  Scenario: Register user with valid credentials

    Given register page is opened
    When user submits valid credentials
    Then user is redirected to the home page and notified with successful registration message

  Scenario Outline: Register user with invalid credentials

    Given register page is opened
    When user submits invalid credentials: <username> and <password>
    Then user is notified with <exception> message

  Examples: Credentials
    | username | password | exception |
    | johnDoe | johnDoeHere | The password is too similar to the username |
    | !Sansa! | shoudPass | Enter a valid username                        |
    | easy_password | qwerty | This password is too common                |
