from behave import given, when, then


@given('register page is opened')
def step_impl(context):
    br = context.browser
    br.get(context.base_url + '/register')
    assert br.find_element_by_name('csrfmiddlewaretoken').is_enabled(), 'CSRF middleware token is missing'


@when('user submits valid credentials')
def step_impl(context):
    br = context.browser
    br.find_element_by_name('username').send_keys('validuser')
    br.find_element_by_name('password1').send_keys('thisisThebestPW123')
    br.find_element_by_name('password2').send_keys('thisisThebestPW123')
    br.find_element_by_name('submit').click()


@then('user is redirected to the home page and notified with successful registration message')
def step_impl(context):
    br = context.browser
    assert br.current_url.endswith('/')
    assert 'Welcome onboard validuser!' in br.page_source


@when('user submits invalid credentials: {username} and {password}')
def step_impl(context, username, password):
    br = context.browser
    br.find_element_by_name('username').send_keys(username)
    br.find_element_by_name('password1').send_keys(password)
    br.find_element_by_name('password2').send_keys(password)
    br.find_element_by_name('submit').click()


@then('user is notified with {exception} message')
def step_impl(context, exception):
    br = context.browser
    assert br.current_url.endswith('/register')
    assert exception in br.page_source
