from behave import given, when, then
from pet_app.test.factories.user import UserFactory


@given('an anonymous user')
def step_impl(context):
    u = UserFactory(username='foo', email='foo@example.com')
    u.set_password('bar')
    u.save()


@when('user submits a valid login page')
def step_impl(context):
    br = context.browser
    br.get(context.base_url + '/login/')
    assert br.find_element_by_name('csrfmiddlewaretoken').is_enabled(), 'CSRF middleware token is missing'

    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar')
    br.find_element_by_name('submit').click()


@when('user submits an invalid login page')
def step_impl(context):
    br = context.browser

    br.get(context.base_url + '/login/')
    assert br.find_element_by_name('csrfmiddlewaretoken').is_enabled(), 'CSRF middleware token is missing'

    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar-is-invalid')
    br.find_element_by_name('submit').click()


@then('user is notified with login failed message')
def step_impl(context):
    br = context.browser

    assert br.current_url.endswith('/login/')
    assert 'Please enter a correct username and password' in br.page_source, 'Error message is not visible on page'
